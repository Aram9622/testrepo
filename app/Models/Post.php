<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;
use TCG\Voyager\Models\Category;

class Post extends \TCG\Voyager\Models\Post
{
    use HasFactory;

    protected $attributes = ['image'];

    public function getImageAttribute(){
        return asset('storage/'.$this->attributes['image']);
    }

    protected $hidden = [
        "created_at"
    ];

    public function comment(){
        return $this->hasMany(Comment::class);
    }

    public function post_comment(){
        return $this->belongsToMany(Comment::class, 'post_comments');
    }



}
